package views;

import javax.swing.*;
import java.awt.*;

public class ClockPanel extends JPanel {
    private static final int _width = 250;
    private static final int _height = 250;
    private static final int _clockX = 10;
    private static final int _clockCenterX = _width / 2;
    private static final int _clockWidth = _width - 2 * _clockX;
    private static final int _clockHeight = _height - 2 * _clockX;

    private Hand _hourHand;
    private Hand _minuteHand;
    private Hand _secondHand;

    public ClockPanel() {
        super(new BorderLayout());
        setPreferredSize(new Dimension(_width, _height));

        int clockY = _height / 2 - _clockHeight / 2;
        int clockCenterY = clockY + _height / 2 - _clockX;

        _hourHand = new Hand(_clockCenterX, clockCenterY, _clockCenterX, clockY + 50);
        _minuteHand = new Hand(_clockCenterX, clockCenterY, _clockCenterX, clockY + 25);
        _secondHand = new Hand(_clockCenterX, clockCenterY, _clockCenterX, clockY + 10);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHints(
                new RenderingHints(
                        RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON));

        drawClockFace((Graphics2D) g);
        drawClockHands((Graphics2D) g);
    }

    public void setHands(double hourAngle, double minuteAngle, double secondAngle) {
        _hourHand.rotate(hourAngle);
        _minuteHand.rotate(minuteAngle);
        _secondHand.rotate(secondAngle);
    }

    private void drawClockFace(Graphics2D g) {
        int clockY = getHeight() / 2 - _clockHeight / 2;
        int clockCenterY = clockY + _height / 2 - _clockX;

        g.setStroke(new BasicStroke(2));
        g.drawOval(_clockX, clockY, _clockWidth, _clockHeight);
        g.fillOval(_clockCenterX - 5, clockCenterY - 5, 10, 10);
    }

    private void drawClockHands(Graphics2D g) {
        int originClockY = _height / 2 - _clockHeight / 2;
        int clockY = getHeight() / 2 - _clockHeight / 2;

        Point vector = new Point(0, clockY - originClockY);

        // second hand
        Point start = _secondHand.getStart();
        Point end = _secondHand.getEnd();
        g.setStroke(new BasicStroke(1));
        g.drawLine(
                (int) start.getX(),
                (int) (start.getY() + vector.getY()),
                (int) end.getX(),
                (int) (end.getY() + vector.getY()));

        // minute hand
        start = _minuteHand.getStart();
        end = _minuteHand.getEnd();
        g.setStroke(new BasicStroke(2));
        g.drawLine(
                (int) start.getX(),
                (int) (start.getY() + vector.getY()),
                (int) end.getX(),
                (int) (end.getY() + vector.getY()));

        // hour hand
        start = _hourHand.getStart();
        end = _hourHand.getEnd();
        g.setStroke(new BasicStroke(4));
        g.drawLine(
                (int) start.getX(),
                (int) (start.getY() + vector.getY()),
                (int) end.getX(),
                (int) (end.getY() + vector.getY()));
    }

    private class Hand {
        private Point _start;
        private Point _end;
        private Point _endZeroPosition;

        public Hand(int x1, int y1, int x2, int y2) {
            this(new Point(x1, y1), new Point(x2, y2));
        }

        public Hand(Point start, Point end) {
            _start = start;
            _end = end;
            _endZeroPosition = new Point(end);
        }

        public Point getStart() {
            return _start;
        }

        public Point getEnd() {
            return _end;
        }

        public void rotate(double angle) {
            int offsetX = (int)_start.getX();
            int offsetY = (int)_start.getY();

            int x1 = (int)_endZeroPosition.getX() - offsetX;
            int y1 = (int)_endZeroPosition.getY() - offsetY;

            int x2=((int)(x1*Math.cos(angle*Math.PI/180)))-((int)(y1* Math.sin(angle*Math.PI/180)));
            int y2=((int)(x1*Math.sin(angle*Math.PI/180)))+((int)(y1* Math.cos(angle*Math.PI/180)));

            _end.setLocation(x2 + offsetX, y2 + offsetY);
        }
    }
}
