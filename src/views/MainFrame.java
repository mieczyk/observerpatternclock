package views;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    private AnalogClockView _analogClockView;
    private DigitalClockView _digitalClockView;

    public MainFrame(String windowName, AnalogClockView analogClockView, DigitalClockView digitalClockView) {
        super(windowName);

        _analogClockView = analogClockView;
        _digitalClockView = digitalClockView;

        initComponents();
    }

    private void initComponents() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(_analogClockView.getPanel(), BorderLayout.NORTH);
        mainPanel.add(_digitalClockView.getPanel(), BorderLayout.CENTER);


        setContentPane(mainPanel);

        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
}