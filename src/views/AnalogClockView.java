package views;

import controllers.ClockController;

import javax.swing.*;

public class AnalogClockView {
    private ClockController _controller;
    private ClockPanel _clockPanel;

    public AnalogClockView(ClockController controller) {
        _controller = controller;

        initComponents();
    }

    private void initComponents() {
        _clockPanel = new ClockPanel();

        _controller.onUpdate(t -> {
            double hourAngle = (60 * t.getHour() + t.getMinute()) / 2;
            double minuteAngle = 6 * t.getMinute();
            double secondAngle = 6 * t.getSceond();

            _clockPanel.setHands(hourAngle, minuteAngle, secondAngle);
        });
    }

    public JPanel getPanel() {
        return _clockPanel;
    }
}