package views;

import controllers.ClockController;

import javax.swing.*;
import java.awt.*;

public class DigitalClockView {
    private ClockController _controller;
    private JPanel _mainPanel;
    private JLabel _clockLabel;

    public DigitalClockView(ClockController controller) {
        _controller = controller;

        initComponents();
    }

    private void initComponents() {
        _mainPanel = new JPanel(new BorderLayout());
        _mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        _clockLabel = new JLabel("00:00:00");
        _clockLabel.setHorizontalAlignment(JLabel.CENTER);
        _mainPanel.add(_clockLabel, BorderLayout.CENTER);

        _controller.onUpdate(t ->
                _clockLabel.setText(
                    String.format("%02d:%02d:%02d", t.getHour(), t.getMinute(), t.getSceond())));
    }

    public JPanel getPanel() {
        return _mainPanel;
    }
}