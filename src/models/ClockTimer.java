package models;

import java.time.LocalDateTime;

public class ClockTimer extends Subject {
    private int _hour;
    private int _minute;
    private int _second;

    public int getHour() {
        return _hour;
    }

    public int getMinute() {
        return _minute;
    }

    public int getSecond() {
        return _second;
    }

    public void tick() {
        LocalDateTime now = LocalDateTime.now();

        _hour = now.getHour();
        _minute = now.getMinute();
        _second = now.getSecond();

        notifyObservers();
    }
}
