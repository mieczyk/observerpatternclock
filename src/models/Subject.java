package models;

import java.util.ArrayList;
import java.util.List;

// Methods in Java are virtual by default.
public abstract class Subject {
    private List<Observer> _observers;

    public Subject() {
        _observers = new ArrayList<Observer>();
    }

    public void attach(Observer observer) {
        _observers.add(observer);
    }

    public void detach(Observer observer) {
        _observers.remove(observer);
    }

    public void notifyObservers() {
        for (Observer observer : _observers) {
            observer.update(this);
        }
    }
}
