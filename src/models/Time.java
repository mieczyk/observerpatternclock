package models;

public class Time {
    private int _h, _m, _s;

    public Time(int h, int m, int s) {
        _h = h; _m = m; _s = s;
    }

    public int getHour() {
        return _h;
    }

    public int getMinute() {
        return _m;
    }

    public int getSceond() {
        return _s;
    }
}
