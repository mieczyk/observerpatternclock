package controllers;

import models.ClockTimer;
import models.Observer;
import models.Subject;
import models.Time;

import java.util.function.Consumer;

public class ClockController extends Observer {
    private ClockTimer _clockTimer;
    private Consumer<Time> _handler;

    public ClockController(ClockTimer clockTimer) {
        _clockTimer = clockTimer;
        _clockTimer.attach(this);

        _handler = null;
    }

    public void onUpdate(Consumer<Time> handler) {
        _handler = handler;
    }

    @Override
    public void update(Subject subject) {
        if(subject instanceof ClockTimer && _handler != null) {
            int h = _clockTimer.getHour();
            int m = _clockTimer.getMinute();
            int s = _clockTimer.getSecond();

            _handler.accept(new Time(h, m, s));
        }
    }
}
