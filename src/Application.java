import controllers.ClockController;
import models.ClockTimer;
import views.AnalogClockView;
import views.DigitalClockView;
import views.MainFrame;

import javax.swing.*;
import java.util.TimerTask;

public class Application {
    public static void main(final String[] args) {
        SwingUtilities.invokeLater(() -> {
            // Register dependencies
            ClockTimer clockTimer = new ClockTimer();
            DigitalClockView digitalClockView = new DigitalClockView(new ClockController(clockTimer));
            AnalogClockView analogClockView = new AnalogClockView(new ClockController(clockTimer));

            // Create main frame
            MainFrame frame = new MainFrame("Clock Timer", analogClockView, digitalClockView);
            frame.setVisible(true);

            // Run timer
            java.util.Timer timer = new java.util.Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    clockTimer.tick();
                    frame.repaint();
                }
            }, 500, 500);
        });
    }
}
